#!/usr/bin/env sh

BUILD_SUBTARGET="${1:-generic}"
BIN_DIR_DEFAULT="${IMAGEBUILDER_HOME}/bin"
BIN_DIR="${2:-${BIN_DIR_DEFAULT}}"

if [[ "${1}" == "--help" ]]; then
	echo "Usage: ${0} <generic|tiny> [bin-output-directory]" >&2
	echo "Build images of OpenWrt machines into the bin output directory." >&2
	echo "The bin output directory to build the images into can be set by the second argument (it defaults to ${BIN_DIR_DEFAULT})." >&2
	exit 1
fi

# DISABLED as we do not need any of the cutom packages anymore
#cat >> "${IMAGEBUILDER_HOME}/repositories.conf" <<END
#
### Custom online repository at http://rychly-edu.gitlab.io/openwrt-packages/
#src/gz rychly_openwrt_packages http://rychly-edu.gitlab.io/openwrt-packages/releases/${OPENWRT_VERSION}/packages/${OPENWRT_ARCH}/custom
#
#END

for I in ./machines/*.sh; do
	MACHINE=
	PROFILE=
	PACKAGES=
	SUBTARGET=
	. "${I}"
	[ "${SUBTARGET}" != "${BUILD_SUBTARGET}" ] && continue
	FILES="${I%.sh}"
	MACHINE_DEFAULT="${FILES##*/}"
	MACHINE_BIN_DIR="${BIN_DIR}/${MACHINE:=${MACHINE_DEFAULT}}"
	mkdir -p "${MACHINE_BIN_DIR}"
	cd "${IMAGEBUILDER_HOME}"
	echo "Building a custom image by ${I} ..."
	make image \
		PROFILE="${PROFILE}" \
		PACKAGES="${PACKAGES}" \
		FILES="${FILES}" \
		BIN_DIR="${MACHINE_BIN_DIR}" \
		EXTRA_IMAGE_NAME="custom" \
	|| exit 1
	cd -
done
