# OpenWrt packages

https://rychly-edu.gitlab.io/openwrt-images/

Custom images of OpenWrt (source code and builds).

## Dependencies

For building the OpenWrt images, it is recommended to use the [OpenWrt Image Builder in a Docker image](https://gitlab.com/rychly-edu/docker/docker-openwrt-imagebuilder).
